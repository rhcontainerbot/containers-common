#!/usr/bin/env bash

export BASE_URL="https://raw.githubusercontent.com/containers"

export COMMON_BRANCH="v0.40.1"
export COMMON_URL="$BASE_URL/common/$COMMON_BRANCH"

export IMAGE_BRANCH="v5.13.0"
export IMAGE_URL="$BASE_URL/image/$IMAGE_BRANCH"

export PODMAN_BRANCH="v3.2.2"
export PODMAN_URL="$BASE_URL/podman/$PODMAN_BRANCH"

export SKOPEO_BRANCH="v1.3.1"
export SKOPEO_URL="$BASE_URL/skopeo/$SKOPEO_BRANCH"

export SHORTNAMES_BRANCH="main"
export SHORTNAMES_URL="$BASE_URL/shortnames/$SHORTNAMES_BRANCH"

export STORAGE_BRANCH="v1.32.2"
export STORAGE_URL="$BASE_URL/storage/$STORAGE_BRANCH"

# Install empty mounts.conf for debian/ubuntu
#echo "Fetching mounts.conf from c/common..."
#wget -O mounts.conf $COMMON_URL/pkg/subscriptions/mounts.conf

echo "Fetching seccomp.json from c/common..."
wget -O seccomp.json $COMMON_URL/pkg/seccomp/seccomp.json

echo "Fetching containers.conf from c/common..."
wget -O containers.conf $COMMON_URL/pkg/config/containers.conf

echo "Fetching containers.conf.5.md from c/common..."
wget -O containers.conf.5.md $COMMON_URL/docs/containers.conf.5.md

echo "Fetching containers-registries.conf.d.5.md from c/image..."
wget -O containers-registries.conf.d.5.md $IMAGE_URL/docs/containers-registries.conf.d.5.md

echo "Fetching containers-auth.json.5.md from c/image..."
wget -O containers-auth.json.5.md $IMAGE_URL/docs/containers-auth.json.5.md

echo "Fetching containers-signature.5.md from c/image..."
wget -O containers-signature.5.md $IMAGE_URL/docs/containers-signature.5.md

echo "Fetching containers-transports.5.md from c/image..."
wget -O containers-transports.5.md $IMAGE_URL/docs/containers-transports.5.md

echo "Fetching containers-certs.d.5.md from c/image..."
wget -O containers-certs.d.5.md $IMAGE_URL/docs/containers-certs.d.5.md

echo "Fetching containers-registries.d.5.md from c/image..."
wget -O containers-registries.d.5.md $IMAGE_URL/docs/containers-registries.d.5.md

echo "Fetching containers-registries.conf.5.md from c/image..."
wget -O containers-registries.conf.5.md $IMAGE_URL/docs/containers-registries.conf.5.md

echo "Fetching registries.conf from c/image..."
wget -O registries.conf $IMAGE_URL/registries.conf

echo "Fetching containers-policy.json.5.md from c/image..."
wget -O containers-policy.json.5.md $IMAGE_URL/docs/containers-policy.json.5.md

echo "Fetching containers-mounts.conf.5.md from c/podman..."
wget -O containers-mounts.conf.5.md $PODMAN_URL/docs/source/markdown/containers-mounts.conf.5.md

echo "Fetching shortnames.conf from c/shortnames..."
wget -O shortnames.conf $SHORTNAMES_URL/shortnames.conf

echo "Fetching policy.json from c/skopeo..."
wget -O policy.json $SKOPEO_URL/default-policy.json

echo "Fetching default.yaml from c/skopeo..."
wget -O default.yaml $SKOPEO_URL/default.yaml

echo "Fetching storage.conf from c/storage..."
wget -O storage.conf $STORAGE_URL/storage.conf

echo "Fetching containers-storage.conf.5.md from c/storage..."
wget -O containers-storage.conf.5.md $STORAGE_URL/docs/containers-storage.conf.5.md


echo "Changing storage.conf..."
sed -i -e 's/^driver.*=.*/driver = "overlay"/' -e 's/^mountopt.*=.*/mountopt = "nodev,metacopy=on"/' \
        storage.conf

echo "Changing seccomp.json..."
[ `grep "keyctl" seccomp.json | wc -l` == 0 ] && sed -i '/\"kill\",/i \
                                "keyctl",' seccomp.json
sed -i '/\"socketcall\",/i \
                                "socket",' seccomp.json

echo "Changing registries.conf..."
sed -i 's/^#.*unqualified-search-registries.*=.*/unqualified-search-registries = ["docker.io", "quay.io"]/g' \
        registries.conf

